# App To Do List

- Projeto desenvolvido com base no Ignite da Rocketseat

## Exec

- Instale as dependências com: `yarn` ou `npm install`
- Inicie o projeto com: `yarn dev` ou `npm run dev`
- Acesse em: http://localhost:5173

## Projeto final

<img src="./src/assets/capa/capa-1.png" />

---

<img src="./src/assets/capa/capa-2.png" />
