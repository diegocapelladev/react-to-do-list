import styled, { css } from 'styled-components'

export const Heading = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-top: 6.4rem;
    margin-bottom: 2.4rem;

    div {
      display: flex;
      align-items: center;
      gap: 1rem;
    }
`

export const Created = styled.div`
  ${({ theme }) => css`
    color: ${theme.colors.blue};
    font-size: ${theme.font.sizes.small};

    span {
      display: flex;
      align-items: center;
      justify-content: center;
      color: ${theme.colors.gray200};
      background-color: ${theme.colors.gray400};
      font-size: ${theme.font.sizes.xsmall};
      padding: 0.5rem 1rem;
      border-radius: 50%;
    }
  `}
`

export const Concluded = styled.div`
  ${({ theme }) => css`
    color: ${theme.colors.purple};
    font-size: ${theme.font.sizes.small};

    span {
      display: flex;
      align-items: center;
      justify-content: center;
      color: ${theme.colors.gray200};
      background-color: ${theme.colors.gray400};
      font-size: ${theme.font.sizes.xsmall};
      padding: 0.5rem 1rem;
      border-radius: 9rem;
    }
  `}
`
export const Content = styled.main`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 1.2rem;
`

export const Empty = styled.div`
  ${({ theme }) => css`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 1rem;
    border-top: 1px solid ${theme.colors.gray400};
    padding-top: 6.4rem;
    color: ${theme.colors.gray300};
  `}
`
