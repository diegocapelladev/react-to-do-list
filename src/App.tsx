import { ClipboardText } from 'phosphor-react'
import { useEffect, useState } from 'react'
import { Container } from './components/container/Container'
import Header from './components/Header'
import Input from './components/Input'
import Task from './components/Task'

import * as S from './styles/App'

export type TasksProps = {
  id: string
  title: string
  isCompleted: boolean
}

export function App() {
  const [tasks, setTasks] = useState<TasksProps[]>([])

  const getSavedTasks = () => {
    const saved = localStorage.getItem('task')
    if (saved) {
      setTasks(JSON.parse(saved))
    }
  }

  useEffect(() => {
    getSavedTasks()
  }, [])

  const setSaveTasks = (newTasks: TasksProps[]) => {
    setTasks(newTasks)
    localStorage.setItem('task', JSON.stringify(newTasks))
  }

  const taskQuantity = tasks.length
  const completedTasks = tasks.filter((task) => task.isCompleted).length

  const addTask = (taskTitle: string) => {
    setSaveTasks([
      ...tasks,
      {
        id: crypto.randomUUID(),
        title: taskTitle,
        isCompleted: false
      }
    ])
  }

  const handleTaskCompleted = (taskId: string) => {
    const newTasks = tasks.map((task) => {
      if (task.id === taskId) {
        return {
          ...task,
          isCompleted: !task.isCompleted
        }
      }
      return task
    })
    setTasks(newTasks)
  }

  const handleDeleteTask = (taskId: string) => {
    const newTask = tasks.filter((task) => task.id !== taskId)
    setTasks(newTask)
  }

  return (
    <>
      <Header />
      <Container>
        <Input onAddTask={addTask} />
        <S.Heading>
          <S.Created>
            <h2>Tarefas criadas</h2>
            <span>{taskQuantity}</span>
          </S.Created>

          <S.Concluded>
            <h2>Concluídas</h2>
            <span>
              {completedTasks} de {taskQuantity}
            </span>
          </S.Concluded>
        </S.Heading>

        <S.Content>
          {tasks.length > 0 ? (
            tasks.map((task, index) => (
              <Task
                key={index}
                task={task}
                onDelete={handleDeleteTask}
                onCompleted={handleTaskCompleted}
              />
            ))
          ) : (
            <S.Empty>
              <ClipboardText size={52} />
              <h3>Você ainda não tem tarefas cadastradas</h3>
              <p>Crie tarefas e organize seus itens a fazer</p>
            </S.Empty>
          )}
        </S.Content>
      </Container>
    </>
  )
}
