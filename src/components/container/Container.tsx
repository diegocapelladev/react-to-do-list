import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Container = styled.div`
  ${({ theme }) => css`
    margin: 0 auto;
    display: grid;
    max-width: ${theme.grid.container};

    ${media.lessThan('large')`
      padding: 0 calc(${theme.grid.gutter} / 4);
    `}
  `}
`
