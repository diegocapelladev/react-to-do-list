import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  margin-top: -2.7rem;
  height: 5.4rem;
  display: flex;
  align-items: center;
  gap: 1rem;
`

export const Input = styled.input`
  ${({ theme }) => css`
    height: 100%;
    width: 100%;
    background-color: ${theme.colors.gray500};
    border-radius: ${theme.border.radius};
    color: ${theme.colors.gray300};
    border: 1px solid ${theme.colors.gray700};
    padding: ${theme.spacings.xsmall};
    outline: none;

    &::placeholder {
      color: ${theme.colors.gray300};
    }

    &:focus {
      border: 1px solid ${theme.colors.gray300};
    }
  `}
`

export const Button = styled.button`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    gap: 0.8rem;
    background-color: ${theme.colors.blue};
    color: ${theme.colors.white};
    height: 100%;
    padding: ${theme.font.sizes.medium};
    border-radius: ${theme.border.radius};
    border: none;
    font-size: ${theme.font.sizes.small};
    font-weight: bold;
    cursor: pointer;
    transition: all 0.2s;

    &:hover {
      filter: brightness(0.9);
    }
  `}
`
