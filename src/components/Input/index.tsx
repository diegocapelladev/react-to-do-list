import { PlusCircle } from 'phosphor-react'
import { ChangeEvent, FormEvent, useState } from 'react'

import * as S from './styles'

export type InputProps = {
  onAddTask: (taskTitle: string) => void
}

const Input = ({ onAddTask }: InputProps) => {
  const [title, setTitle] = useState('')

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault()

    onAddTask(title)
    setTitle('')
  }

  const onChangeTitle = (event: ChangeEvent<HTMLInputElement>) => {
    setTitle(event.target.value)
  }

  return (
    <form onSubmit={handleSubmit}>
      <S.Wrapper>
        <S.Input
          type="text"
          value={title}
          onChange={onChangeTitle}
          placeholder="Adicione uma nova tarefa"
          autoComplete="off"
        />
        <S.Button>
          Criar <PlusCircle size={15} />
        </S.Button>
      </S.Wrapper>
    </form>
  )
}

export default Input
