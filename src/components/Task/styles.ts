import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    justify-content: space-between;
    gap: 2rem;
    width: 73.6rem;
    padding: 1rem;
    background-color: ${theme.colors.gray500};
    border: 1px solid ${theme.colors.gray400};
    border-radius: ${theme.border.radius};
    color: ${theme.colors.gray100};
  `}
`

export const Check = styled.input`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    justify-content: center;
    appearance: none;
    width: 2.4rem;
    height: 2.4rem;
    border: 0.2rem solid ${theme.colors.blue};
    background: transparent;
    border-radius: 50%;
    outline: none;
    cursor: pointer;

    &:focus {
      box-shadow: 0 0 0.5rem ${theme.colors.purple};
    }

    &:before {
      content: '';
      width: 1.2rem;
      height: 1.2rem;
      border-radius: 50%;
      background: ${theme.colors.purple};
      transition: opacity 0.1s ease-in-out;
      opacity: 0;
      position: absolute;
    }

    &:checked {
      &::before {
        opacity: 1;
      }
    }
  `}
`

export const Text = styled.p``

export const Delete = styled.button`
  ${({ theme }) => css`
    background: transparent;
    border: none;
    color: ${theme.colors.gray300};
    cursor: pointer;
    transition: all 0.2s;

    &:hover {
      color: ${theme.colors.danger};
    }
  `}
`
