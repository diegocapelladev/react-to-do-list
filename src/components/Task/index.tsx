import { Trash } from 'phosphor-react'
import { useState } from 'react'
import { TasksProps } from '../../App'
import * as S from './styles'

export type TaskProps = {
  task: TasksProps
  onDelete: (taskId: string) => void
  onCompleted: (taskId: string) => void
}

const Task = ({ task, onDelete, onCompleted }: TaskProps) => {
  const [check, setCheck] = useState(task.isCompleted)

  const onChange = () => {
    const status = !check
    setCheck(status)
  }

  return (
    <S.Wrapper>
      <div style={{ display: 'flex', gap: '1rem', alignItems: 'center' }}>
        <S.Check
          type="checkbox"
          checked={check}
          onChange={onChange}
          onClick={() => onCompleted(task.id)}
        />
        {task.isCompleted ? (
          <S.Text style={{ color: '#808080' }}>
            <s>{task.title}</s>
          </S.Text>
        ) : (
          <S.Text>{task.title}</S.Text>
        )}
      </div>
      <S.Delete>
        <Trash size={25} onClick={() => onDelete(task.id)} />
      </S.Delete>
    </S.Wrapper>
  )
}

export default Task
