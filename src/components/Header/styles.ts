import styled, { css } from 'styled-components'

export const Wrapper = styled.header`
  ${({ theme }) => css`
    height: 20rem;
    background-color: ${theme.colors.gray700};
    display: flex;
    align-items: center;
    justify-content: center;
  `}
`
