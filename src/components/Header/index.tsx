import * as S from './styles'

import Logo from '../../assets/images/Logo.png'

const Header = () => {
  return (
    <S.Wrapper>
      <img src={Logo} alt="" />
    </S.Wrapper>
  )
}

export default Header
